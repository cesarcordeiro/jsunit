'use strict';

function TestCase(testName) {
	this.testName = testName;
}

TestCase.prototype.setUp = function() {}
TestCase.prototype.tearDown = function() {}

TestCase.prototype.assert = function (condition) {
	if(!condition) {
		throw new Error()
	}
}

TestCase.prototype.assertEquals = function(expected, actual) {
	new TestCase("assert").assert(expected === actual);
}

TestCase.prototype.run = function(result) {
	result.testStarted();

	this.setUp();

	try {
		var method = this[this.testName];
		method();
	} catch (error) {
		result.testFailed();
	}

	this.tearDown();
}

TestCase.createParentOf = function(obj) {
	obj.prototype = Object.create(TestCase.prototype);
	obj.prototype.constructor = obj;
}

function WasRun(testName) {

	TestCase.call(this, testName);

	var self = this;

	this.testMethod = function() {
		self.logg = self.logg + "testMethod ";
	}

	this.setUp = function() {
		self.logg = "setUp ";
	}

	this.tearDown = function () {
		self.logg = self.logg + "tearDown ";
	}

	this.testBrokenMethod = function () {
		throw new Error();
	}

}

function TestResult() {

	var runCount = 0;
	var errorCount = 0;

	this.testStarted = function() {
		runCount = runCount + 1;
	}

	this.testFailed = function () {
		errorCount = errorCount + 1;
	}

	this.summary = function() {
		return runCount + " run, " + errorCount + " failed";
	}
}

function TestSuite() {

	var tests = [];

	this.add = function (testCase) {
		tests.push(testCase);
	}

	this.run = function (testResult) {
		for(var i in tests) {
			tests[i].run(testResult);
		}
	}

}

function TestCaseTest(testMethod) {

	TestCase.call(this, testMethod);

	var self = this;
	var result = null;

	this.setUp = function() {
		result = new TestResult();
	}

	this.testTemplateMethod = function() {
		var test = new WasRun("testMethod");
		test.run(result);
		self.assertEquals("setUp testMethod tearDown ", test.logg);
	}

	this.testResult = function() {
		var test = new WasRun("testMethod");
		test.run(result);
		self.assertEquals("1 run, 0 failed", result.summary());
	}

	this.testFailedResult = function () {
		var test = new WasRun("testBrokenMethod");
		test.run(result);
		self.assertEquals("1 run, 1 failed", result.summary());
	}

	this.testFailedResultFormatting = function () {
		result.testStarted();
		result.testFailed();
		self.assertEquals("1 run, 1 failed", result.summary());
	}

	this.testCaseSuite = function () {
		var suite = new TestSuite();
		suite.add(new WasRun("testMethod"));
		suite.add(new WasRun("testBrokenMethod"));
		
		suite.run(result);

		self.assertEquals("2 run, 1 failed", result.summary());
	}

}

TestCase.createParentOf(WasRun);
TestCase.createParentOf(TestCaseTest);
